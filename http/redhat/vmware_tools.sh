#!/bin/bash
######################################################
# Install VMware tools
#######################################################
set -ex
mkdir {{ mountdir }}
mount -o loop {{ iso }} {{ mountdir }}
tar xzvf {{ mountdir }}/VMwareTools*.tar.gz -C {{ workdir }}
cd {{ workdir }}/vmware-tools-distrib/
yum install fuse-libs perl -y
./vmware-install.pl -default
umount {{ mountdir }}
rmdir {{ mountdir }}
rm -rf {{ workdir }}/vmware-tools-distrib/
cat <<EOF > /etc/sysconfig/modules/vmware.modules
#!/bin/bash

modprobe vmhgfs
modprobe vmxnet
EOF
chmod +x /etc/sysconfig/modules/vmware.modules