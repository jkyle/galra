#!/bin/bash
set -ex
shopt -s extglob

function setup() {
	mkdir -p {{ workdir }}
	cd {{ workdir }}
	export PATH=/usr/local/bin:$PATH
}

function get_sources() {
	git clone --depth 1 git://git.openvswitch.org/openvswitch -b master

	curl -O http://ftp.gnu.org/gnu/autoconf/autoconf-latest.tar.gz
	tar xzf autoconf-latest.tar.gz

	git clone --depth 1 git://github.com/mininet/mininet
}

function depends() {

	#runtime deps
	yum install centos-release-SCL -y
	yum install telnet graphviz python27 -y

	# Build deps
	yum install gcc make python-devel openssl-devel kernel-devel  \
			    kernel-debug-devel autoconf automake rpm-build git \
			    redhat-rpm-config libtool -y
}

function cleanup() {
	yum remove gcc make python-devel openssl-devel kernel-devel graphviz \
			    kernel-debug-devel autoconf automake rpm-build git \
			    redhat-rpm-config libtool -y
}

function install_autoconf() {
	cd {{ workdir }}/autoconf-*
	./configure && make && make install
}

function build_openvswitch() {
	cd {{ workdir }}/openvswitch
	mkdir -p {{ workdir }}/rpmbuild/SOURCES

	./boot.sh
	./configure --prefix=/usr --localstatedir=/var
	make dist

	cp openvswitch-*.tar.gz {{ workdir }}/rpmbuild/SOURCES/
	tar xzf openvswitch-*.tar.gz && cd openvswitch-*

	cp rhel/openvswitch-kmod.files {{ workdir }}/rpmbuild/SOURCES
	rpmbuild --define '_topdir {{ workdir }}/rpmbuild'  \
			 -bb \
			 --define "kversion `uname -r`" \
			 rhel/openvswitch.spec

	rpmbuild --define '_topdir {{ workdir }}/rpmbuild'  \
			 -bb \
			 -D "kversion `uname -r`" \
			 -D "kflavors default debug" \
             rhel/openvswitch-kmod-rhel6.spec
}

function install_openvswitch() {
	cd {{ workdir }}/rpmbuild/RPMS/x86_64
	rpm -i kmod-openvswitch-!(debug*)
	rpm -i openvswitch-!(debug*)
}

function install_mininet() {
  source /opt/rh/python27/enable
  cd {{ workdir }}/mininet
  make mnexec
  cp mnexec /usr/local/bin
  cp bin/mn /usr/local/bin
  python setup.py build
  python setup.py install
}

function create_start_script() {
	cat <<EOF > /home/{{ username }}/start.sh
#!/bin/bash

export PATH=/usr/local/bin:$PATH
source /opt/rh/python27/enable
mn $@
EOF

	chmod +x /home/{{ username }}/start.sh
}

function cleanup() {
	yum remove gcc python-devel openssl-devel kernel-devel \
		       kernel-debug-devel autoconf automake rpm-build git \
		       redhat-rpm-config libtool -y
}

function main() {
	setup
	depends
	get_sources
	install_autoconf
	build_openvswitch
	install_openvswitch
	install_mininet
	create_start_script
	cleanup
}

main
