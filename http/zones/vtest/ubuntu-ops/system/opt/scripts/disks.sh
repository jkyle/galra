#!/bin/bash

lvs | grep lv_var  && lvremove -f /dev/vg_data/lv_var
lvs | grep lv_home && lvremove -f /dev/vg_data/lv_home
vgs | grep vg_data && vgremove -f vg_data
pvs | grep sdb1    && pvremove -f /dev/sdb1
pvs | grep sdb2    && pvremove -f /dev/sdb2

### Partition and setup filesystem;
parted /dev/sdb --script -- mklabel gpt
parted /dev/sdb --script -- mkpart primary ext4 1 50%
parted /dev/sdb --script -- mkpart primary ext4 50% 100%

if [ -e /dev/sdb1 ];then
  pvcreate /dev/sdb1 /dev/sdb2
  vgcreate vg_data /dev/sdb1
fi

if [ ! -e /dev/mapper/vg_data-lv_var ];then
  lvcreate -n lv_var -l 50%VG vg_data
  lvcreate -n lv_home -l 50%VG vg_data
  mkfs.ext4 /dev/mapper/vg_data-lv_var  > /dev/null 2>&1
  mkfs.ext4 /dev/mapper/vg_data-lv_home > /dev/null 2>&1
fi

if [ ! -e /var2 ];then
  mkdir -p /var2 /home2
  mount /dev/mapper/vg_data-lv_var /var2
  mount /dev/mapper/vg_data-lv_home /home2
  rsync -a /var/ /var2/
  rsync -a /home/ /home2/
  umount /var2
  umount /home2
  rmdir /var2 /home2
  #mount -a
fi

if ! grep "vg_data-lv_var" /etc/fstab > /dev/null 2>&1; then
  echo "/dev/mapper/vg_data-lv_var /var  ext4  defaults  0 2" >> /etc/fstab
fi

if ! grep "vg_data-lv_home" /etc/fstab > /dev/null 2>&1; then
  echo "/dev/mapper/vg_data-lv_home /home  ext4  defaults  0 2" >> /etc/fstab
fi
