#!/bin/bash
set -e
DEBUG=true
CLEAN=true
project="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"
echo "ISO_TARGET: ${ISO_TARGET}"
target=${ISO_TARGET:-${project}/http/zones/vtest/ubuntu-ops/}

source ${target}/config.sh
image=${image:-/var/downloads/custom.iso}
iso=${iso:-/var/downloads/ubuntu-12.04.3-server-amd64.iso}
preseed=${target}/preseed.cfg
#iso=/var/downloads/mini.iso
mntdir=/tmp/iso.mnt.${RANDOM}
custom=/tmp/iso.custom.${RANDOM} initrd=/tmp/iso.initrd.${RANDOM}

function debug() {
  if [ $DEBUG = "true" ]; then
    echo "DEBUG: $@"
  fi
}

function set_vars() {
echo "initrd: ${mntdir}/initrd.gz"
  if [ -e ${mntdir}/initrd.gz ];then
    echo "Configuring for mini.iso...."
    isolinux=isolinux.cfg
    txtcfg=txt.cfg
    kernel="linux"
    isobin=isolinux.bin
    input=${mntdir}/initrd.gz
  else
    echo "Configuring for ubuntu server..."
    isolinux=isolinux/isolinux.cfg
    txtcfg=isolinux/txt.cfg
    kernel="/install/vmlinuz"
    isobin=isolinux/isolinux.bin
    input=${mntdir}/install/initrd.gz
  fi
  output=${custom}/initrd.gz
}

function create_isolinux() {
  cat <<EOF > $isolinux
# D-I config version 2.0
include menu.cfg
default vesamenu.c32
prompt 0
timeout 10
EOF
}

function create_txtcfg() {
  cat <<EOF > $txtcfg
default install
label install
  menu label ^Install
  kernel $kernel
  append  file=/preseed.cfg console-setup/ask_detect=false console-setup/layout=us debian-installer/locale=en_US netcfg/choose_interface=auto initrd=/initrd.gz debconf/frontend=noninteractive priority=critical -- quiet
EOF
}

function setup() {
  mkdir -vp $mntdir $custom
  mount -t iso9660 -o loop $iso $mntdir
  set_vars  # must be first!
  echo "Creating working directories...."
  rsync -a $mntdir/ $custom

  cd $custom
  echo en > lang

  create_isolinux
  create_txtcfg
}

function create_iso() {
  echo "Creating iso...."

  debug "Including $isobin"
  mkisofs -r \
          -V Custom \
          -cache-inodes \
          -J \
          -l \
          -input-charset utf-8 \
          -b $isobin \
          -c boot.cat \
          -no-emul-boot \
          -boot-load-size 4 \
          -boot-info-table \
          -o $image \
          $custom
}

function create_initrd() {
  echo "Creating custom initrd.gz..."
  mkdir -vp $initrd
  cd $initrd

  gzip -d < $input | cpio --extract \
                             --make-directories \
                             --no-absolute-filenames
  cp -v ${preseed} preseed.cfg
  cp -rv ${target}/system system


  debug Creating ${output}
  debug "find . | cpio -H newc --create | gzip -9 > ${output}"
  find . | cpio -H newc --create | gzip -9 > ${output}
  cd $custom
  md5sum `find -type f` > md5sum.txt
}

function cleanup() {
  echo "Cleaning up...."
  if [ $CLEAN = "true" ];then
    umount $mntdir
    rmdir $mntdir
    rm -rf $mntdir $custom
    rm -rf $initrd
  fi
  if [ $DEBUG = "true" ];then
    debug umount $mntdir
    debug rmdir $mntdir
    debug rm -rf $mntdir $custom
    debug rm -rf $initrd
  fi
}

setup
create_initrd
create_iso
cleanup
