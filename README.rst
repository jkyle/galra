****************
Acknowledgements
****************

.. role:: red
.. role:: blue
.. role:: green

The project heavily relies on the following upstream projects. They do most of
the heavy lifting.


* `Invoke`_
* `Packer`_
* `Flask`_
* `Gunicorn`_


Invoke
======

Invoke is a build system similar to make. To list available commands

.. code-block:: sh

    % inv --list

Might return something like

.. code-block:: sh

    Available tasks:

	build
	clean_cache
	killserver
	makedirs
	server


Packer
======

Packer is a tool for building images for use in a variety of virtual
environments including VMWare, Virtualbox, Openstack, EC2, and more.


Flask
=====

Flask is a micro web framework. It's used to serve templatized preseed,
kickstart, and post flight scripts to packer. Packer has a basic built in web
server, but as of the start of this project it did not support templates.


***********
Quick Start
***********

Description
===========

This project was built targeting a vmware environment. However, virtualbox
support would be trivial to add. In either case, the vms should use a shared
network. In vmware, this means the vm's ip is NAT'd behind that of the host
machine. It also means there should be a shared network. This shared network is
required so the guest machines may reach the template server hosting preseed
files, post flight scripts, etc.


You should note which ip your host is using on that network so you can pass that
value to the build invocation.

Virtual Environment
===================


Installation
============

First, you'll need to install dependencies. Thee's a script in the utilities
directory of this project to validate your environment.

If you run

.. code-block:: sh

    % ./utils/checkdeps.py

You might see something like

    | :blue:`Checking for packer:` :green:`Found!`
    | :blue:`Checking for flask:` :red:`Not Found!`
    | :blue:`Checking for gunicorn:` :green:`Found!`
    | :blue:`Checking for invoke:` :green:`Found!`
    | :blue:`Checking for requests:` :green:`Found!`

Packer
------

The easiest way to install packer on OS X is with `Homebrew`_.

.. code-block:: sh

    % brew update
    % brew install packer

For those using linux, you can refer to your distros package manager or install
from source.

Python Libraries
----------------

The project provides a requirements.txt file. To install all python dependencies
run.

.. code-block:: sh

    pip install -r requirements.txt

Still using easy_install? You can cat the file for the list....or install `pip`_
like a sane human being.

The Template Server
===================

Next, start the template server.

.. code-block:: sh

    % inv server


This will start a server that listens on 0.0.0.0:5000 by default. You'll need to
note your local shared ip so you can pass that to the build command.

Build an Image
==============

.. code-block:: sh

    % inv build -t templates/redhat.json --host="http://172.16.216.1:5000"

This will build the servers as declared in the redhat.json template. The
172.16.216.1 address is that of our host's shared ip.

By default, images are built in headless mode. If you'd like to see what's going
on, you can disable headless mode with the --no-headless flag.

.. code-block:: sh

    % inv build -t templates/redhat.json \
    			--host="http://172.16.216.1:5000" \
    			--no-headless

Using a Caching Proxy
=====================

If you're building a lot of machines or developing a new build over many
iterations, you can vastly speed up your development cycles by using a caching
server for deb or rpm repositories. The build system supports configuration for
a proxy server through the --proxy switch. Further, the resulting system will be
configured to use that proxy server as well.

.. code-block:: sh

    % inv build -t templates/redhat.json \
    			--host="http://172.16.216.1:5000" \
    			--no-headless \
    			--proxy=http://172.16.101.175:8000/

In this example, an rpm proxy server is running at 172.16.101.175 port 8000.
Configuration of a proxy server is left as an exercise for the user. However,
the `squid-deb-proxy`_ package for debian based systems is incredibly simple to
configure for both deb and rpm caching.

The Vagrantfile provided with this project contains a vm definition that
configures a caching proxy configured for puppetlabs, centos, and apt package
repositories.

.. code-block:: sh

    % vagrant up proxy


.. _Packer: https://github.com/mitchellh/packer
.. _Flask: http://flask.pocoo.org
.. _Invoke: http://docs.pyinvoke.org/en/latest/
.. _Homebrew: http://brew.sh
.. _pip: http://www.pip-installer.org/en/latest/
.. _gunicorn: http://gunicorn.org
.. _squid-deb-proxy: https://launchpad.net/squid-deb-proxy
